import argparse, textwrap, sys
from game_of_life.main import main

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description=textwrap.dedent(
            """\
        """
        ),
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument("filepath", type=str, help="location of file to load from")

    arg = parser.parse_args()
    arg = arg.filepath

    main(arg)
