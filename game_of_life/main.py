import random
import os

DIRECTIONS = [(0, 1), (0, -1), (1, 0), (-1, 0), (1, 1), (-1, 1), (1, -1), (-1, -1)]


def dead_state(width, height):
    board = [[0 for _ in range(height)] for _ in range(width)]
    return board


def random_state(width, height):
    board = dead_state(width, height)

    for x in range(width):
        for y in range(height):
            board[x][y] = random.randint(0, 1)

    return board


def render(state):
    width = len(state)
    height = len(state[0])

    print("+-+".replace("-", "-" * width))

    for y in range(0, height):
        line = "|"
        for x in range(0, width):
            cell = state[x][height - 1 - y]
            if cell == 0:
                line += " "
            else:
                line += "#"
        line += "|"
        print(line)

    print("+-+".replace("-", "-" * width))


def next_board_state(state):
    width = len(state)
    height = len(state[0])

    new_state = dead_state(width, height)

    for y in range(0, height):
        for x in range(0, width):
            cell = state[x][height - 1 - y]
            neighbors = 0

            for direction in DIRECTIONS:
                check = (
                    ((x + direction[0]) % width),
                    ((y + direction[1]) % height),
                )
                if state[check[0]][height - 1 - check[1]]:
                    neighbors += 1

            if cell:
                if neighbors < 2:
                    new_state[x][height - 1 - y] = 0
                elif neighbors < 4:
                    new_state[x][height - 1 - y] = 1
                else:
                    new_state[x][height - 1 - y] = 0
            else:
                if neighbors == 3:
                    new_state[x][height - 1 - y] = 1

    return new_state


def main(filepath):
    with open(filepath) as f:
        lines = [line.rstrip() for line in f]

    width = len(lines[0])
    height = len(lines)
    state = dead_state(width, height)

    for y in range(0, height):
        for x in range(0, width):
            state[x][height - 1 - y] = int(lines[y][x])

    os.system("cls" if os.name == "nt" else "clear")
    render(state)

    while True:
        next_state = next_board_state(state)
        state = next_state
        os.system("cls" if os.name == "nt" else "clear")
        render(state)
        if input() == "q":
            break
